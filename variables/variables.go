package main

import "fmt"

func main() {
	// String
	var a = "initial"
	fmt.Println(a)

	// Integer
	var b, c int = 1, 2
	fmt.Println(b, c)

	// Float
	var d float64 = 3.7
	fmt.Println(d)

	// Boolean
	var e = true
	fmt.Println(e)

	// Instantiating a variable without a value makes its value 0
	var f int
	fmt.Println(f)

	// The := syntax is shorthand for declaring and initializing a variable
	g := "Apple"
	fmt.Println(g)
}
