
package main

import "fmt"
import "rsc.io/quote"
// import "rsc.io/quote/v3"

func main() {
  fmt.Println("Hello. You are amazing")
  fmt.Println(quote.Go())
  fmt.Println(quote.Glass())

  // fmt.Println(quote.GoV3())
  // fmt.Println(quote.GlassV3())
}
